# CFLAGS=--std=c17 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG
CFLAGS=--std=gnu17 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG
BUILDDIR=build
OBJDIR=obj
SRCDIR=src
CC=gcc

.PHONY: all clean

all: $(OBJDIR)/mem.o $(OBJDIR)/util.o $(OBJDIR)/mem_debug.o $(OBJDIR)/main.o
	$(CC) -o $(BUILDDIR)/main $^

build:
	mkdir -p $(OBJDIR)
	mkdir -p $(BUILDDIR)

$(OBJDIR)/mem.o: $(SRCDIR)/mem.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(OBJDIR)/mem_debug.o: $(SRCDIR)/mem_debug.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(OBJDIR)/util.o: $(SRCDIR)/util.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(OBJDIR)/main.o: $(SRCDIR)/main.c build
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -rf $(BUILDDIR)
	rm -rf $(OBJDIR)

