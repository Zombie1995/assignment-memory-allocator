#include "mem.h"
#include "mem_internals.h"
#include <stdio.h>

#define FIRST_BLOCK_SIZE 1
#define SECOND_BLOCK_SIZE 121
#define THIRD_BLOCK_SIZE 112
#define BIG_BLOCK_SIZE 10000
#define VERY_BIG_BLOCK_SIZE 200000

static int test1_normal_allocation() {
    printf("test1_normal_allocation:\n");
    debug_heap(stdout, HEAP_START);
    void *block = _malloc(FIRST_BLOCK_SIZE);
    debug_heap(stdout, HEAP_START);
    printf("\n");

    struct block_header const* header1 = HEAP_START;
    int ret = header1->capacity.bytes == (FIRST_BLOCK_SIZE > 24 ? FIRST_BLOCK_SIZE : 24)
              && header1->is_free == 0;
    
    _free(block);
    _free((uint8_t*)HEAP_START + offsetof(struct block_header, contents));
    
    return ret;
}

static int test2_free_one_block() {
    printf("test2_free_one_block:\n");
    void *block1 = _malloc(FIRST_BLOCK_SIZE);
    void *block2 = _malloc(SECOND_BLOCK_SIZE);
    void *block3 = _malloc(THIRD_BLOCK_SIZE);
    debug_heap(stdout, HEAP_START);
    _free(block1);
    debug_heap(stdout, HEAP_START);
    printf("\n");
    
    struct block_header const* header1 = HEAP_START;
    struct block_header const* header2 = header1->next;
    struct block_header const* header3 = header2->next;
    int ret = header1->is_free == 1
              && header2->is_free == 0
              && header3->is_free == 0;

    _free(block2);
    _free(block3);
    _free((uint8_t*)HEAP_START + offsetof(struct block_header, contents));
    
    return ret;
}

static int test3_free_multiple_blocks() {
    printf("test3_free_multiple_blocks:\n");
    void *block1 = _malloc(FIRST_BLOCK_SIZE);
    void *block2 = _malloc(SECOND_BLOCK_SIZE);
    void *block3 = _malloc(THIRD_BLOCK_SIZE);
    debug_heap(stdout, HEAP_START);
    _free(block1);
    _free(block3);
    debug_heap(stdout, HEAP_START);
    _free(block2);
    debug_heap(stdout, HEAP_START);
    printf("\n");

    struct block_header const* header1 = HEAP_START;
    struct block_header const* header2 = header1->next;
    int ret = header1->is_free == 1
              && header2->is_free == 1;

    _free((uint8_t*)HEAP_START + offsetof(struct block_header, contents));
    
    return ret;
}

static int test4_new_region_extends_old() {
    printf("test4_new_region_extends_old:\n");
    debug_heap(stdout, HEAP_START);
    void *block = _malloc(BIG_BLOCK_SIZE);
    _free(block);
    debug_heap(stdout, HEAP_START);
    printf("\n");

    struct block_header const* header1 = HEAP_START;
    int ret = header1->capacity.bytes > REGION_MIN_SIZE;

    _free((uint8_t*)HEAP_START + offsetof(struct block_header, contents));

    return ret;
}


#include <errno.h>
#include <unistd.h>
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }
static void create_rubbish_region(void *start_addr) {
    start_addr = ((uint8_t*)start_addr);
    size_t rubbish_region_length = round_pages(REGION_MIN_SIZE);
    // void *rubbish_region_start = mmap( start_addr, rubbish_region_length, PROT_NONE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, 0, 0 );
    // void *rubbish_region_start = mmap( start_addr, rubbish_region_length, PROT_NONE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0 );
    // void *rubbish_region_start = mmap( start_addr, rubbish_region_length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0 );
    void *rubbish_region_start = mmap( start_addr, rubbish_region_length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, 0, 0 );
    printf("%p\n", rubbish_region_start);
    rubbish_region_start = mmap( start_addr, rubbish_region_length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, 0, 0 );
    printf("%p\n", rubbish_region_start);
    printf("PLS READTHIS ABOUT TEST5\n");
    printf("PLS READTHIS ABOUT TEST5\n");
    printf("PLS READTHIS ABOUT TEST5\n");
    printf("PLS READTHIS ABOUT TEST5\n");
    printf("PLS READTHIS ABOUT TEST5\n");
    printf("PLS READTHIS ABOUT TEST5\n");
    printf("PLS READTHIS ABOUT TEST5\n");
    printf("PLS READTHIS ABOUT TEST5\n");
    printf("PLS READTHIS ABOUT TEST5\n");
    printf("PLS READTHIS ABOUT TEST5\n");
    printf("PLS READTHIS ABOUT TEST5\n");
    printf("PLS READTHIS ABOUT TEST5\n");
    printf("PLS READTHIS ABOUT TEST5\n");
    printf("As you can see above mapping from the same address will not throw errors \nSo i don't know how to create rubbish region to make creating blocks in isolated region\n");
    if (rubbish_region_start == MAP_FAILED) perror("wtf ");
    void *rubbish_region_end = (uint8_t*)rubbish_region_start + rubbish_region_length;
    printf("Mapping supposed to be from %p\n", start_addr);
    printf("Rubbish region created from %p to %p\n", rubbish_region_start, rubbish_region_end);
}
static int test5_problems_with_region_extension() {
    printf("test5_problems_with_region_extension:\n");
    void *block_end_address = (uint8_t*)HEAP_START + size_from_capacity(((struct block_header*)HEAP_START)->capacity).bytes;
    create_rubbish_region(block_end_address);
    void *block = _malloc(VERY_BIG_BLOCK_SIZE);
    debug_heap(stdout, HEAP_START);
    _free(block);
    _free((uint8_t*)HEAP_START + offsetof(struct block_header, contents));
    debug_heap(stdout, HEAP_START);
    printf("\n");
    return 1;
}

int main(int argc, char *argv[]) {
    (void)argc;(void)argv;
    heap_init(1);

    int test_passed = 0;
    test_passed += test1_normal_allocation();
    test_passed += test2_free_one_block();
    test_passed += test3_free_multiple_blocks();
    test_passed += test4_new_region_extends_old();
    test_passed += test5_problems_with_region_extension();

    printf("Test passed:\n%d\n", test_passed);
    return 0;
}